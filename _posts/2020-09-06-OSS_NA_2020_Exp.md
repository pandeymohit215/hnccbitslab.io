---
layout: post
title: Open Source Summit, NA 2020
subtitle: Virtual Experience
css: /assets/css/styles.css
image: https://images.youracclaim.com/images/e6066b96-c59d-49b6-87cc-d8873022e84f/blob.png
author: Raj Sinha

tags : [experience, Open Source]
---

![header](https://raw.githubusercontent.com/hnccbits/cdn/master/images/blog/2020/Page-1-Image-1.jpg)

I was glad to be selected as a LiFT scholar by The Linux Foundation.
The Linux Foundation Training (LiFT) Scholarship Program to provide opportunities to up-and-coming developers and sysadmins who show promise for helping shape the future of Linux and open source software but do not otherwise have the ability to attend training courses.

Year after year, Open Source Summit North America is a sell-out success story. This is the one event that really doesn’t have to worry too much about self-promotion. Every year, this event presented by The Linux Foundation provides a unique opportunity for cross-collaboration within the open source community, and this year was no different...except that there was no sprinting between breakout sessions or sneaking in the back after a session started, as this year’s event was all virtual. Programmers, developers and like-minded peers enjoyed insightful keynotes, informational breakout sessions and more, all from the comfort of their own homes. 

Open Source Summit North America (OSS NA) (formerly LinuxCon) is the leading conference for developers, architects, and other technologists – as well as open source community and industry leaders – to collaborate, share information, learn about the latest technologies and gain a competitive advantage by using innovative open solutions.This Summit connects the open source ecosystem under one roof. It covers cornerstone open source technologies; helps ecosystem leaders to navigate open source transformation with the Diversity Empowerment Summit and tracks on business and compliance; and delves into the newest technologies and latest trends touching open source, including networking, cloud-native, edge computing, AI and much more. It is an extraordinary opportunity for cross-pollination between the developers, sysadmins, DevOps professionals and IT architects driving the future of technology.

The OSS + ELC North America 2020 Virtual Experience provided participants with numerous interactive experiences included:
- 230 sessions with live speaker Q&A
- Attendee networking via 1:1 chat and group networking lounges across a range of topics
- Platform access for one year post-event, all virtually.

![speakers-images](https://raw.githubusercontent.com/hnccbits/cdn/master/images/blog/2020/Page-2-Image-2.jpg)

Sessions took place at scheduled times and  Speakers were available for a live Q&A with conference sessions & tutorial attendees. Attendees could build a personalized agenda of sessions they want to attend and receive notifications before sessions start. 

Apart from keynotes given by some high-profile Linux people like Linus Torvalds (creator of Linux and Git ) , Grace Francisco ( Vice President, Worldwide Developer Relations and Education at MongoDB) etc. Personally I liked the live webinar on how the Coronavirus pandemic has accelerated the adoption of A.I. across sectors from finance to healthcare. How can we use the richness of datasets, while protecting the individuals and communities they belong to? Edge A.I. & Federated Learning: how multi-party computation can build intelligent healthcare systems without moving data by Frederick Kautz, Head of Edge Infrastructure from doc.ai, the mobile health company.

After the event ended, all attendees got the access to the sessions and other content and resources from the event platform for one year. I am watching the recorded sessions, and this was the best online event I attended entire lockdown. It was a great experience attending the OSS NA as one of the LiFT scholars .

Additionally, all recorded sessions are transferred to The Linux Foundation YouTube channel  so that anyone  can  freely access it from [here](https://www.youtube.com/watch?v=vjKPw5FqleM&list=PLbzoR-pLrL6oyIqGsEZdb1E4pWzWn9qOZ)

<iframe width="100%" height="100%" src="https://www.youtube.com/embed/vjKPw5FqleM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" style="overflow:hidden;height:100%;width:100%" allowfullscreen></iframe>

If you think that the Open Source Summit is all about technology then you are completely wrong, the aim of these conferences / summits  is to show what a tremendous change can be made with a structured approach to preparation, including rehearsing, mentorship, and support, with a little help from technology.

Happy Learning ❤️️                      
**Raj Sinha**                    
IT 2k16                       
HnCC               
