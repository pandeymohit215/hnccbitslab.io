---
layout: post
title: BITCode June Round Editorial
subtitle: Editorial

css: /assets/css/style.css
author: Krit Raj
tags: [BITCode June, solutions, editorial, competitive programming]
---

**Hi.** Thank you for participating in the BITCode June Round. We hope that you enjoyed the problems we prepared for you. We would like to apologize for the late editorial. Incase, if you weren’t able to solve them, here are the solutions with tested code that you can read to figure out what went wrong in the contest.

&nbsp;

Upsolving the problems is the most important thing to do after a contest. Here they are:-

# **Vamika's Exam**

-  **Problem Link:** [Click Here](https://www.hackerrank.com/contests/hncc-bitcode-june/challenges/good-student)

-  **Author:** [Pranav Singh](https://codeforces.com/profile/pranav_singh)

-  **Topics Covered:** Maths

### **Problem Setter's Solution:**

```c++
#include <bits/stdc++.h>
using namespace std;
int main() {
  int t;
   cin>>t;
   while(t--)
   {
       int n;cin>>n;
       int arr[n];bool flag=true;
       for(int i=0;i<n;i++)
       {
           cin>>arr[i];
           if(arr[i]%2==1)flag=false;//checking if any number is odd
       }
       cout<<(flag?"YES":"NO")<<endl;
   }
   return 0;
}
```

# **Marriage**

-  **Problem Link:** [Click Here](https://www.hackerrank.com/contests/hncc-bitcode-june/challenges/prafullas-marriage-)

-  **Author:** [Pranav Singh](https://codeforces.com/profile/pranav_singh)

-  **Topics Covered:** Maths, Implementation

### **Problem Setter's Solution:**

```c++
#include <bits/stdc++.h>
using namespace std;
int main() {
  int t;
   cin>>t;
   while(t--)
   {
       long long int n;long long int k;
     cin>>n>>k;
   long long int q=n/k;
    if(q%k==0)
        cout<<"NO"<<endl;
   else
       cout<<"YES"<<endl;
    }
   return 0;
}
```

# **Hogwarts Express**

-  **Problem Link:** [Click Here](https://www.hackerrank.com/contests/hncc-bitcode-june/challenges/hogwarts-express)

-  **Author:** [Unnat Kumar](https://codeforces.com/profile/unnatkumar)

-  **Topics Covered:** Maths

### **Problem Setter's Solution:**

```c++
#include<bits/stdc++.h>
using namespace std;
int main() {
   string s;
   cin>>s;
   int n=s.length();
   int ans=n/2;
   if(n%2)ans++;
   int count0=count(s.begin(),s.end(),'0');
   if(n%2 && count0>=n-1){
       ans--;
   }
   cout<<ans<<"\n";
   return 0;
}
```

# **More Money!**

-  **Problem Link:** [Click Here](https://www.hackerrank.com/contests/hncc-bitcode-june/challenges/more-candies-2)

-  **Author:** [Divyansh Sahu](https://codeforces.com/profile/divyansh_sahu)

-  **Topics Covered:** Dynamic Programming, BFS

### **Problem Setter's Solution:**

```c++
#include <bits/stdc++.h>
using namespace std;
const int N = 1001;
int main() {
 vector<int> d(N, N);
 d[1] = 0;
 for (int i = 1; i < N; ++i) {
   for (int x = 1; x <= i; ++x) {
     int j = i + i / x;
     if (j < N) d[j] = min(d[j], d[i] + 1);
   }
 }
 int t;
 cin >> t;
 while (t--) {
   int n, k;
   cin >> n >> k;
   vector<int> b(n), c(n);
   for (int &x : b) cin >> x;
   for (int &x : c) cin >> x;
   int sum = 0;
   for (int x : b) sum += d[x];
   k = min(k, sum);
   vector<int> dp(k + 1, 0);
   for (int i = 0; i < n; ++i) {
     for (int j = k - d[b[i]]; j >= 0; j--) {
       dp[j + d[b[i]]] = max(dp[j + d[b[i]]], dp[j] + c[i]);
     }
   }
   cout << *max_element(dp.begin(), dp.end()) << '\n';
 }
}
```

# **Chocolate Tree**

-  **Problem Link:** [Click Here](https://www.hackerrank.com/contests/hncc-bitcode-june/challenges/date-6)

-  **Author:** [Prafulla Shekhar](https://codeforces.com/profile/prafullashekhar)

-  **Topics Covered:** Greedy, Two Pointers

### **Problem Setter's Solution:**

```c++
#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
// typedef long double ld;
typedef vector<ll> vl;
#define v vector
// #define uset unordered_set
// #define umap unordered_map
#define trav(e, a) for (auto &e : a)
#define f(i, a, b) for (int i = a; i < b; i++)
#define take(a)                        \
   for (int i = 0; i < a.size(); i++) \
   cin >> a[i]
#define all(x) x.begin(), x.end()
#define endl "\n"
#define sp " "
#define pb push_back
#define mp make_pair
#define lb lower_bound
#define ub upper_bound
#define bs binary_search
#define eb emplace_back
// #define svz(x) (int)(x).size()
#define ff first
#define ss second
#define sz(x) sizeof(x) / sizeof(int)
#define mod 1000000007
#define yes "YES"
#define no "NO"
void solve()
{
   int n; cin>>n;
   vl a(n); take(a);
   int i=1, j=n-2, l=1, r=1, mx=a[0];
   while(i<n && a[i]>mx){ l++; mx=a[i]; i++; }
   mx=a[n-1];
   while(j>=0 && a[j]>mx){ r++; mx=a[j]; j--; }
   if(a[i-1]<a[n-1]){ l+=r; }
   if(a[j+1]<a[0]){ r+=l; }
   // cout<<l<<sp<<r<<endl;
   if(l%2==0 && r%2==0){ cout<<"Prafull"; }
   else cout<<"Pranav";
}
int main(void)
{
   ios_base::sync_with_stdio(false);
   cin.tie(NULL);
   cout.tie(NULL);
   /*#ifndef ONLINE_JUDGE
       freopen("./input.txt", "r", stdin);
       freopen("./output.txt", "w", stdout);
   #endif*/
   int t = 1;
   // cin >> t;
   while (t--)
   {
       // cout<<"Case #"<<t<<": ";
       solve();
       cout << endl;
   }
   return 0;
}
```

Happy Learning!!!

<!-- WRITTEN BY: Krit Raj -->

<!-- Tags: BITCode June  solutions   editorial   competitive programming -->
