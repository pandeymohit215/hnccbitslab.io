---
layout: post
title: "HACK-O-VERSE"
subtitle: An HnCC Organized Tech-Fest Event
css: /assets/css/styles.css
author: Abhishek Mondal
tags : [Hack-O-Verse , BIT Code , Hackathon ,Uxathon , Gamathon , Capture the flag , Tech-Fest]
---

BIT Sindri's Tech Mahotsav 2023 was a grand     success, with students showcasing their innovation and creativity in various events and competitions organized by the **Hackathon and Coding Club**, a weekday event celebrating the college's diverse range of societies and clubs.

The event featured a range of exhibitions and competitions that enabled students to learn practical skills and gain valuable experience in their respective fields. The participation of professors and seniors provided students with the necessary guidance and support to excel in their endeavors

A thrilling and inspirational inauguration ceremony established the tone for the whole event and served as its official beginning. The comments of our director, **Dr. D.K. Singh**, sir, motivated the attendees, and the ceremony to light the lamps set the tone for the remainder of the event. Positive energy, teamwork, and invention were fostered, allowing the participants to express their ideas and develop ground-breaking solutions fully. The director also stressed the value of teamwork and urged the attendees to collaborate to provide original answers to pressing problems.

The Hack-O-Verse includes **BIT CODE**, **HACKATHON**, **UXATHON**, **GAMATHON**, and **CAPTURE THE FLAG**, attracting participants from various colleges and universities, and making it a highly competitive event. BIT CODE gives a great approach to evaluating and improving coding skills.

This contest will involve the participants working in a team to develop solutions to challenges.

The Hackathon challenged participants to develop innovative solutions to real-world problems, while the **UXATHON** focused on designing user-friendly and visually appealing interfaces. The **GAMATHON** brought together participants who were passionate about game development, and they worked tirelessly to create fun and engaging games.

Hack-O-Verse tends to end with the **CAPTURE THE FLAG** online event, which was a highly anticipated online debugging contest. The participants were supposed to debug and submit their code within time.

The participants collaborated in teams, sharing ideas and insights to build their projects. The event was an excellent opportunity for the students to learn from their peers, improve their coding skills, and gain valuable experience.

The judges were highly impressed with the project's quality, and it wasn't easy to select the winners. The winners received cash prizes and certificates, and their projects were highly praised for their creativity and technical excellence.

In conclusion, Under the banner of **Hack-O-Verse**, which includes _BIT CODE_, _Hackathon_, _UXathon_, _Gamathon_, and _Capture the Flag_, were highly engaging and challenging events that provided an excellent platform for participants to showcase their skills and creativity. The event was a great success, and it left everyone feeling inspired and energized to pursue their passion for technology and innovation. It was a testament to the college's commitment to nurturing its students' talent and preparing them for a successful career in the technology industry.

### **DAY 0: "Beginning of War of Programming Contests"**  
<br/>

# **BIT CODE:**
<br/>

We are happy to share the success of our online coding competition, _BIT Code_, organized by **Hackathon and Coding Club** on the HackerRank platform. The event provided an excellent opportunity for coders to compete against each other in a challenging and rewarding environment.  

The online coding competition took place on February 25, 2023, with a wide variety of participants from various colleges, and 164 participants participated. The competition was open to all skill levels and included a variety of programming challenges designed to test participants' skills with various programming languages and algorithms.

We hosted the event on the Hacker Rank platform, which provided a user-friendly interface for participants to submit solutions. The platform allowed our expert judges to quickly and accurately evaluate each submission based on code accuracy, efficiency, and elegance. The competition ran for 2 hours; the top 10 rankers got certificates of achievement, and the rest got certificates of participation.

Here Judging panel includes:

1. Mr. Tapan Kumar Nayak
   
2. Mr. Vikash Kumar Singh
 
3. Mr. Prashant Kumar


We are glad to report that the event was successful and received positive participant and community feedback. We can continue to hold successful events in the future.

### **DAY 1:" Hosted two events, namely HACKATHON & UXATHON."**
<br/>

# **HACKATHON :**
<br/>
Our club hosted a 36-hour Hackathon Under Hack-O-verse on February 26, 2023, where 52 teams registered, with a maximum of 5 members per team. The Hackathon aimed to foster creativity, collaboration, and innovation among participants by solving real-world problems by creating new technological solutions, such as software applications or websites. The event allowed participants to showcase their skills, learn new techniques, and network with others in the industry.  

During the event, 20+ teams participated in the Hackathon from 10 am on the first day until 10 pm the next day. The participants worked tirelessly to develop innovative solutions to real-world problems using their technical expertise and creative thinking. The Hackathon also served as a platform for participants to meet and collaborate with like-minded individuals, share ideas, and learn from each other. As the world continues to evolve, so do the challenges communities, businesses, and individuals face. Hackathons have become a popular platform for innovators to come together and solve some of these challenges.

This Hackathon's problem statements range from campus safety to urban waste management, administrative processes, persons with disabilities, and worksite safety and productivity.  

In conclusion, this Hackathon presents a unique opportunity for participants to tackle some of the most pressing challenges facing our communities, businesses, and individuals.With creativity, innovation, and expertise, these problem statements can be turned into solutions that can make a real difference.

The judging panel consisted of several distinguished professors and professionals in the field of technology, namely, Prof. Vikash Kumar Singh, Prof. Shesh Narayan Sahu, Prof. Sachin Kumar, Prof. Parbati Mahanto, Prof. Md. Akram Khan, Dr. Dinesh Kumar Prabhakar, Prof. Panjeet Kumar Lenka, Prof. S.C. Dutta, Prof. Tapan Kumar Nayak, Dr. Pooja Sharma, Dr. Anantha Raj.

The judging criteria were based on four key areas: presentation, innovation, changes made according to feedback, and new changes made beyond the input given. Each team had to present its solution to the panel, which evaluated them based on these criteria.

The judges assessed each team based on the quality of their presentation, including their pitch's clarity and persuasiveness. They also evaluated the level of innovation in their solution and the extent to which they incorporated feedback into their work. Finally, the judges looked for new changes that the teams had made beyond the input given.

# **UXATHON:**
<br/>

Under the banner of Hack-O-Verse, UXATHON was conducted on February 26 from 12 pm to 3 pm. That set challenges for the designers to showcase their skills in creating user-centered experiences for different websites through UI/UX.  

The event was conducted in two rounds in which the team's maximum size was two members, but solo participation was also allowed. In the first stage, the participants were asked to redesign the UI on the given topics, such as Spotify, Zomato, Myntra, and Big Basket. Based on creativity and understanding of the topic, top teams were selected.


### **DAY 2: "Continuation of HACKATHON and UXathon, hosted GAMATHON."**
<br/>

On the second day of the Hack-O-Verse, the Hackathon continued with even more enthusiasm and energy from the participants. The morning began with two more mentoring sessions, where experienced mentors provided guidance and feedback to the teams. The mentors shared their expertise on various aspects, such as coding techniques, project management, and user experience design, helping the teams refine their projects further. After the mentoring sessions, the participants continued working on their projects, implementing the feedback they received and making the necessary changes. The teams worked tirelessly, pushing their limits to ensure their projects were as polished and innovative as possible. In the afternoon, the final judging rounds took place, and the judges had the difficult task of selecting the winners from the exceptional projects. The judges evaluated the projects based on their innovation, technical excellence, and relevance to real-world problems. The final presentations were intense and exciting, with the teams showcasing their projects enthusiastically and passionately. The judges asked probing questions, and the teams confidently responded, demonstrating their deep understanding of the projects. The event ended with the announcement of the winners, with the top three teams receiving prizes and certificates.

The winners of the event are `Team Hash Code (Amit Kumar, Akshat Apurva)` in first position, followed by `Team Appdeb (Nilesh Gupta, Aditya Kr. Singh)` in second, and `Team RS Tech (Rohit Sah)` in third. However, all the participants were left with invaluable experience, new skills, and a more profound passion for technology and innovation. Overall, the Hackathon was a resounding success, a testament to the power of collaboration, innovation, and creativity in solving real-world problems. The event left a lasting impact on all those involved, inspiring them to continue pushing the boundaries of what is possible by applying technology.

# **UXathon:**
<br/>

The second round was all about implementing the ideas on any of the given domains, such as Technical, Cultural, Sports, and happenings around the college. In this round, the participants designed the home (landing) page with wireframing, and the complete flow of the design was explained. The entire event was held with proper guidelines under the guidance of Md. Amir Akhtar, Ritesh Kumar, and all other seniors. It provided a platform for the participants to showcase their skills in Adobe XD, Figma, etc.

The winners of the event are `Team Hero (Deepak, Prem Kumar)` in first position, followed by `Team Vajra (Vikash Kumar)` in second, and `Team Fire (Sakcham Singh, Abhishek Kumar)` in third.

The event ended with great success and active participation of the candidates, and the club hopes to conduct more such exciting events in the future

# **GAMATHON:**
<br/>
Game Development is the most exciting thing which comes to mind when we hear "Development." Everyone wants to develop their games with their own rules. Gamathon, a part of the mega event named Hack-O-Verse, was organized on February 27, 2023, from 10:00 am to 4:00 pm. The event consisted of 25 teams who were allowed to showcase their skills and creativity in game development. Each team can have at most 4 participants. The event consisted of two rounds. The first round was conducted online on February 23, where teams had to submit the idea and the working model of their games in online mode. Then the selected will present their games offline. A panel of experts judged the games.

1. PROF. D, K. PRABHAKAR

2. PROF. MD AKRAM KHAN

3. PROF. BHAVESH KUMAR

and some specific audiences were also involved in evaluating so that they could give raw judgment and experts analyzed the games based on their interactivity, interest, and smoothness of play. The game with the most engagement was declared the winner at the end of the day, and the winner was awarded exciting prizes. Also, participation certificates were provided to all the participants. So, here we got to witness a mind-blowing and exciting event in the game development field.

The event ended with great success and enthusiasm the participants and the audiences involved showed. And so we are looking forward to more such fun, creative coding events.

### **DAY 3: "Wrapped up with the hunting of bugs in programming."**
<br/>

# **CAPTURE THE FLAG:**
<br>

It was a highly anticipated online debugging contest organized on the last day of Hack-O-Verse, February 28, from 10 am to 12 pm. The competition was open to everyone, and around 33 teams, each consisting of a maximum of two members, participated. Each team was given a set of codes according to their preferred language - Python, Java, or C++, and the errors were stated along with a description of how the code should work. The participants were supposed to debug and submit their code within the given time, testing their programming language proficiency skills, algorithm design, and time management skills in a highly competitive environment.

We were extremely grateful to have Prof. Panjeet Kumar Panna and Dr. Pooja Sharma on our esteemed judging panel. After a cut-throat competition, the following teams emerged as winners: `Team Bugger (Md. Abid Hussain, Aditya Kr. Singh)` at a first position, followed by `Team Runtime_terror (Suprity Sinha, Dipasa Das)` in second and `Team Tech Nerds (Shruti Kedia, Akarshi Sinha)` at third.

The winners will receive exciting prizes, and the remaining participants will be awarded certificates of participation. From novices to experts, the event provided a platform for everyone to showcase their problem-solving skills. It provided an enriching experience and was a fitting conclusion to the three-day Hackoverse, full of exciting events.

# **GLIMPSES OF THE EVENT**
<br/>

![image1](https://hnccbits.github.io/cdn/images/blog/2023/image1.jpeg)

![image2](https://hnccbits.github.io/cdn/images/blog/2023/image2.jpeg)

![image3](https://hnccbits.github.io/cdn/images/blog/2023/image3.jpeg)

![image4](https://hnccbits.github.io/cdn/images/blog/2023/image4.jpeg)

![image5](https://hnccbits.github.io/cdn/images/blog/2023/image5.jpeg)


![image6](https://hnccbits.github.io/cdn/images/blog/2023/image6.jpeg)

![image7](https://hnccbits.github.io/cdn/images/blog/2023/image7.jpeg)

![image8](https://hnccbits.github.io/cdn/images/blog/2023/image8.jpeg)

![image9](https://hnccbits.github.io/cdn/images/blog/2023/image9.jpeg)

![image10](https://hnccbits.github.io/cdn/images/blog/2023/image10.jpeg)

![image11](https://hnccbits.github.io/cdn/images/blog/2023/image11.jpeg)

![image12](https://hnccbits.github.io/cdn/images/blog/2023/image12.jpeg)

![image13](https://hnccbits.github.io/cdn/images/blog/2023/image13.jpeg)