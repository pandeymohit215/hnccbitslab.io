---
layout: post
title: BITCode August Round Editorial
subtitle:  Solutions
css: /assets/css/styles.css

author: Anjani Kumar

tags : [BITCode August, solutions, editorial, competitive programming]
---

Here is the explanation of solutions with code for the contest [BITCode August Round](https://www.codechef.com/COBI2020)

* [Three or Five](https://discuss.codechef.com/t/chfm35-editorial/76449)
* [Chef and Circle](https://discuss.codechef.com/t/chfcircl-editorial/76465)
* [Pi](https://discuss.codechef.com/t/treepi-editorial/76461)
* [BINode](https://discuss.codechef.com/t/binode-editorial/76451)
* [Chef vs Teacher](https://discuss.codechef.com/t/chefnum1-editorial/76455)

Problem submissions will open soon. Don't forget to upsolve!!!